
######################################################################################
#                                                                                    											     																										#
#          COMPUTATION OF THEORETICAL VALUES                              																													            #
#          FOR THE AMS VALIDATION                                                    																																    #
#          --> phi(r) for a isotropic point source of monokinetic particles       																													    #
#              in an infinite homogeneous medium                                    																															    #
#                                                                              																																					        #
#          from Case, de Hoffmann & Placzek :                                                                                                                                                                    #
#          "Introduction to the theory of neutron diffusion, Los Alamos (1953) "    																									            #
#                                                                                 																																				        #
######################################################################################
from ROOT import *
from math import *
from cmath import atanh
import sys
gROOT.ProcessLine(
"struct Theory_t {\
   Double_t         r;\
   Double_t         r_min;\
   Double_t         r_max;\
   Double_t         c;\
   Double_t         flux;\
};" );

from ROOT import Theory_t 
gl = {}

gl[2] = ([0.577350269189626],
         [1.0])
gl[3] = ([0.0,               0.774596669241483],
         [0.888888888888889, 0.555555555555556])
gl[4] = ([0.339981043584856, 0.861136311594053],
         [0.652145154862546, 0.347854845137454])
gl[5] = ([0.0,               0.538469310105683, 0.906179845938664],
         [0.568888888888889, 0.478628670499366, 0.236926885056189])
gl[6] = ([0.238619186083197, 0.661209386466265, 0.932469514203152],
         [0.467913934572691, 0.360761573048139, 0.171324492379170])
gl[7] = ([0.0,               0.405845151377397, 0.741531185599394, 0.949107912342759],
         [0.417959183673469, 0.381830050505119, 0.279705391489277, 0.129484966168870])
gl[8] = ([0.183434642495650, 0.525532409916329, 0.796666477413627, 0.960289856497536],
         [0.362683783378362, 0.313706645877887, 0.222381034453374, 0.101228536290376]) 

def get_gauss_quad(N):
    xs, ws = gl[N]
    if N%2 == 0:
        xr = [-x for x in xs]; xr.reverse()
        wr = ws[:]; wr.reverse()
    else:
        xr = [-x for x in xs[1:]]; xr.reverse()
        wr = ws[1:]; wr.reverse()
        pass        
    return xr+xs, wr+ws
    
def fixedGauss(f, a, b, N):
    xs, ws = get_gauss_quad(N)
    aux = 0.5*(b-a)
    quad = 0.
    for i in range(N):
        x = a + aux*(xs[i]+1.)
        quad += ws[i]*f(x)
        pass
    return aux*quad

# from Case & Hoffmann, p.71
def gfun(c, mu):
    aux1 = 1 -c*mu*abs(atanh(mu))
    aux2 = 0.5*pi*mu*c
    aux3 = aux1*aux1 + aux2*aux2
    return 1./aux3

def adaptiveGauss(f, a, b, eps_rel, eps_abs, N1=6, first=False):
    N2 = N1+1
    aux1 = fixedGauss(f, a, b, N1)
    aux2 = fixedGauss(f, a, b, N2)
    ave = 0.5*(aux1+aux2)
    err2 = abs(aux1-aux2)
    if  first:
        eps_abs = abs(ave)*eps_rel/100.
        pass
    if abs(ave)> 0.:
        err1 = err2/abs(ave)
    else:
        err1 = err2
        pass
    if err1 < eps_rel or err2 < eps_abs:
        #print(a, b, ave)
        return ave
    #print(a, b, aux1, aux2, "err", err2, "rel", err1)
    mid = 0.5*(a+b)
    val1 = adaptiveGauss(f, a, mid, eps_rel, eps_abs, N1)
    val2 = adaptiveGauss(f, mid, b, eps_rel, eps_abs, N1)
    #print(a, mid, b, val1, val2)
    return val1 + val2

def epsfun(c,r,eps=1.e-10):
    if r==0:
        return 1.
    def fg(mu):
        return gfun(c,mu)*exp(-r/mu)/mu/mu
    aux = adaptiveGauss(fg, 0., 1., eps, eps, 6, first=True)
    return r * exp(r) * aux

# values (but not formula) correspond to Case & Hoffmann, p.60
def dk02dc_p(c, k0):
    v = 1./k0
    v2 = v*v
    val = 0.5*c*v2*v*(c/(v2-1.) - 1./v2)
    return k0/val

def dispers(c,k):
    return k - c*abs(atanh(k))

def root_bisect(fun, x1, x2, eps=1.e-8):
    fmid = fun(x2)
    f = fun(x1)
    if f*fmid>0:
        raise RuntimeError("["+str(x1) +"," + str(x2) + "] must bracket the root")
    if f < 0:
        root = x1
        dx = x2-x1
    else:
        root = x2
        dx = x1-x2
        pass
    while abs(dx) > eps:
        dx *= 0.5
        xmid = root + dx
        fmid = fun(xmid)
        if fmid <= 0:
            root = xmid
        if fmid == 0.:
            break
        pass
    return xmid

def k0(c):
    def fun(k):
        return dispers(c,k)
    val = root_bisect(fun, 0., 1.-1.e-10, 1.e-10)
    return val
    
def phi_asym(c, r):
    if c==0.:
        return 0.
    k = abs(k0(c))
    A = dk02dc_p(c, k)
    val = A*exp(-k*r)/(4.*pi*r)
    return val

def phi_abs(r):
    val = exp(-r) / (4.*pi*r*r)
    return val

def phi(c, r):
    if c ==0.0:
        val = phi_abs(r)
    else:
        val = phi_asym(c, r) + phi_abs(r)*epsfun(c,r)
        pass
    return val

def intphi(c, r1, r2, eps):
    def fun(r):
        return phi(c,r)*4.*pi*r*r
    val = fixedGauss(fun,r1,r2,7)
    return val


if __name__ == "__main__":
    if len(sys.argv)<5 or len(sys.argv)>5:
        raise ValueError("Expected 4 arguments (<sigma_s/sigma_t> <first radius> <last_radius> <step>)")

    c = float(sys.argv[1])
    min = float(sys.argv[2])
    max = float(sys.argv[3])
    step = float(sys.argv[4])

    #preparing ROOT output
    theory = Theory_t()
    ff = TFile("theory.root","RECREATE")
    TheoryTree = TTree("Theory", "Theoretical flux")
    TheoryTree.Branch("Theory",theory,"r/D:r_min/D:r_max/D:c/D:flux/D")
    theory.c = c

    width = step
    radius = min
    while radius<max:
        theory.r = radius+width/2.
        theory.r_min = radius
        theory.r_max = radius+width
        theory.flux = intphi(c,radius,radius+width,1.e-2)
        radius+=step
        print theory.flux
        TheoryTree.Fill()
    TheoryTree.Write()

