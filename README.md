# MENHIR v2.0
Mono Energetic Neutrons and Homogeneous Isotropic Reactions    

## Presentation
The purpose of this code is to estimate the flux (track and coll) in a couple of geometries with the following assumptions:
      - the medium is infinite
      - the source is a point source of monokinetic neutrons localized at point (0,0,0)
      - upon collision with the medium, the only available reactions are absorption and scattering (elastic and isotropic in the laboratory frame)
      
CONTENT
    menhir sources:
	    source files and their headers: 'pointvector[.cc,.hh]', 'geometry[.cc,.hh]', 'geometry[.cc,.hh]' 'geometry[.cc,.hh]' and 'particle[.cc,.hh]'
	    scripts files : 'menhir.cc'
	    cmake folder containing tools to interface with ROOT
    theory folder:
        theory.py and ROOTtheory.py python scripts

## Build
The code is compiled using the following commands :
```
cd $EMPTY_BUILD_DIR$
cmake $BASE_DIR$
make
```
(tested with cmake:3.9.1, make:4.1  and c++:7.2.0)
	
## Usage
The compilation generates an executable file: 'menhir'. Just execute it to get more informations on syntax.
The minimal syntax is just the simulation type (i.e. analog simulation or variance reduction using Adaptive Multilevel Splitting):
* `./menhir AMS`
* `./menhir Analog`

The parameters of the simulation can be passed as an argument using an advanced syntax as follows:
* For AMS:
`./menhir AMS <geometry> <sigma_tot> <sigma_s> <inner radius> <outer radius> <nb_batch> <batch_size> [ams_mode] [k%] [filename]`
* For analog simulation:
`./menhir Analog <geometry> <sigma_tot> <sigma_s> <inner radius> <outer radius> <nb_batch> <batch_size> [filename]`

The default output is stdin. 
If ROOT is available, results will in addition be added in a TTree and dumped to a ROOT file `./last_menhir_run.root`
If a `filename` *not ending* with `.root` is provided, results will be dumped to this file and *not* to stdin.
If a `filename` ending `.root` is provided, results will be added to a TTree and dumped to this file.

### Geometries
Three geometries are available:
* "shell"
    - `./menhir AMS shell <sigma_tot> <sigma_s> <inner radius> <outer radius> ...`
    - Infinite and homogeneous medium of total cross section <sigma_tot> and scattering cross section <sigma_s>
    - Source located at point (0,0,0)
    - Flux in a spherical shell around the source point with inner radius <inner_radius> and outer radius <outer_radius>
    - Theoretical results available FOR SIGMA_TOT=1

* "jawbreaker" 
    - `./menhir AMS jawbreaker <sigma_tot_1> <sigma_s_1> <sigma_tot_2> <sigma_s_2> <inner radius> <outer radius> ...`
    - Infinite medium 
    - Source located at point (0,0,0)
    - Flux in evenly-spaced spherical shells around the source point with alternate compositions: sigma_tot_1-sigma_tot_2-sigma_tot_1-sigma_tot_2...
    - The last shell has an inner radius <inner_radius> and an outer radius <outer_radius>
    - The flux in the last shell should be the same as with the shell geometry IF sigma_tot_1=sigma_tot_2 and sigma_s_1=sigma_s_2

* "delta" 
    - `./menhir AMS delta <sigma_tot_1> <sigma_s_1> <sigma_tot_2> <sigma_s_2> <inner radius> <outer radius> ...`
    - Same as 'jawbreaker' but using delta-tracking (PROBABLY BUGGY...)

### AMS modes
DEFAULT is "position"
* "exact" 
    - Point notes are the distance to the source point  
    - Replicas are splitted at the exact AMS splitting level (probably *between* collision points)
* "position"
    - Point notes are the distance to the source point  
    - Replicas are duplicated at the *first collision point* above the splitting level
* "direction"
    - Point notes take direction into account
    - Replicas are duplicated at the *first collision point* above the splitting level
* "x"
    - Point notes are the 'x' coordinate
    - Replicas are duplicated at the *first collision point* above the splitting level

### Time estimation
Simulation times may seem very low but they only take into account the 'real' simulation, and not the computation
of the mean results and their display or dump.

### Theory
WARNING: Theoretical results only for SIGMA_TOT=1.
Theoretical results are automatically included in output if available

The theory folder contains two scripts:
* "theory.py": Prints the theoretical value for the flux in a shell
* "ROOTtheory.py": Saves the theoretical values for the flux in mutliple shell of fixed width in a ROOT file ("theory.root")

Syntax:
```
python theory/theory.py <sigma_s/sigma_t> <first radius> <last_radius>
python theory/ROOTtheory.py <sigma_s/sigma_t> <first radius> <last_radius> <step>  
```

