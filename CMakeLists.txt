
cmake_minimum_required( VERSION 2.6 FATAL_ERROR )
project(Menhir CXX)
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

set(CMAKE_MODULE_PATH
    ${CMAKE_SOURCE_DIR}/cmake
)
 
file(GLOB sources source/*.cc)
file(GLOB headers source/*.hh)

add_library( menhir_lib  SHARED  ${sources})


find_package(ROOT)
if (ROOT_FOUND)
    message("-- ROOT has been found in directory: ${ROOT_DIR}")
    option(USE_ROOT "Activate ROOT use" ON)
    include_directories(${ROOT_INCLUDE_DIRS})
    target_link_libraries( menhir_lib ${ROOT_LIBRARIES})
else()
    option(USE_ROOT "Activate ROOT use" OFF)
endif()

if(USE_ROOT)
    add_definitions(-DHAS_ROOT)
endif()

add_definitions("-Wall -std=c++11")
add_executable(menhir source/menhir.cc)
target_link_libraries(menhir menhir_lib)

