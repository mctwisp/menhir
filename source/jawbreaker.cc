#include "jawbreaker.hh"
#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

void Jawbreaker::transport(Particle& part)
{ 
    double sigma_tot(sigma_tot_odd), ksi, dist, max_dist;

    ksi = the_rand->uniform(0.,1.);
    if(part.volume%2==0)
        sigma_tot = sigma_tot_even;

    dist = -log(1.-ksi)/sigma_tot;
    max_dist = get_max_flight(part);
    if(dist<max_dist) {
        part.position.move(part.direction,dist);
        if(part.volume>-1) {
            score_track.at(part.volume)+=dist;
            score_coll.at(part.volume)+=1./sigma_tot;
        }
        this->do_collision(part);
    }
    else {
        part.position.move(part.direction,max_dist);
        if(part.volume>-1)
            score_track.at(part.volume)+=max_dist;
        part.volume = part.next_volume;
    }
}
double Jawbreaker::get_distance_to_frontier(const Particle& part, double r_front) const
{
    double r2, mu, delta, d1, d2;

    r2 = part.position.x*part.position.x+part.position.y*part.position.y+part.position.z*part.position.z; 
    mu = part.position.x*part.direction.x+part.position.y*part.direction.y+part.position.z*part.direction.z; 
    delta =  mu*mu - r2 + r_front*r_front;
    if (delta<0)
        return DBL_MAX;

    // distance calculation
    d1 = -mu-sqrt(delta);
    d2 = -mu+sqrt(delta);
    if(d1<=1.e-5)
        d1 = DBL_MAX;
    if(d2<=1.e-5)
        d2 = DBL_MAX;
    return min(d1,d2);
}

double Jawbreaker::get_max_flight(Particle& part) const
{
    double d1, d2, min_dist;
    if(part.volume==source_vol){
         min_dist = this->get_distance_to_frontier(part,frontiers.at(part.volume+1));
         part.next_volume = part.volume+1;
    }
    else if(part.volume<=target_vol){
        d1 = this->get_distance_to_frontier(part,frontiers.at(part.volume));
        d2 = this->get_distance_to_frontier(part,frontiers.at(part.volume+1));
        if(d1<d2) {
            min_dist = d1;
            part.next_volume = part.volume-1;
        }
        else {
            min_dist = d2;
            part.next_volume = part.volume+1;
        }
    }
    else {
        min_dist = this->get_distance_to_frontier(part,frontiers.at(target_vol+1));
        part.next_volume = target_vol;
    }
    return min_dist;
}
void Jawbreaker::do_collision(Particle& part)
{
    double ksi = the_rand->uniform(0.,1.);
    double sigma_tot(sigma_tot_odd),sigma_abs(sigma_abs_odd);
    if(part.volume%2==0) {
        sigma_abs = sigma_abs_even;
        sigma_tot = sigma_tot_even;
    }

    if(ksi<sigma_abs/sigma_tot) {
        part.alive = false;
        part.direction = Vector(0.,0.,0.);
    }
    else
        part.direction = sample_direction();
}
double Jawbreaker::get_importance(const Particle& part) const
{
    double importance;
    if(part.volume==target_vol)
        importance = DBL_MAX;
    else if(part.alive==false && this->ams_mode!=exact)
        importance = -DBL_MAX;
    else if(this->ams_mode==direction) {
        double xx = part.position.x+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.x;
        double yy = part.position.y+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.y;
        double zz = part.position.z+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.z;
        importance = sqrt(xx*xx+yy*yy+zz*zz);
    }
    else if(this->ams_mode==x) 
        importance = part.position.x;
    else if(this->ams_mode==xabs)
        importance = abs(part.position.x);
    else
        importance = part.position.get_r2();
    return importance;
}
double Jawbreaker::get_track(int ivol) const
{
    return score_track.at(ivol);
}
double Jawbreaker::get_coll(int ivol) const
{
    return score_coll.at(ivol);
}
int Jawbreaker::get_nb_volumes() const
{
    return target_vol+1;
}
double Jawbreaker::get_frontier_inf(int ivol) const
{
    return frontiers.at(ivol);
}
double Jawbreaker::get_frontier_sup(int ivol) const
{
    return frontiers.at(ivol+1);
}
void Jawbreaker::update_scores(const Particle& part)
{
}

Jawbreaker::Jawbreaker(double tot1, double scatt1, double tot2, double scatt2, double mini, double maxi, mode m):
    sigma_tot_odd(tot1), sigma_abs_odd(tot1-scatt1), sigma_tot_even(tot2), sigma_abs_even(tot2-scatt2)
{
    type          = jawbreaker;
    ams_mode      = m;
    source_point  = Point(0,0,0);
    source_dir    = sample_direction();
    source_energy = 1.;
    source_vol    = -1;
    frontiers.clear();
    score_track.clear();
    score_coll.clear();
    double layer_width = maxi-mini;
    int nb_layers = floor(mini/layer_width);
    double x = mini-(double) nb_layers*layer_width;
    if(x==0) source_vol = 0;
    for(int i=0;i<nb_layers+1;i++) {
        frontiers.push_back(x+i*layer_width);
        score_track.push_back(0.);
        score_coll.push_back(0.);
    }
    frontiers.push_back(maxi);
    score_track.push_back(0.);
    score_coll.push_back(0.);
    target_vol = nb_layers;
}
