/**************************************************************************
*
*    SIMPLIFIED PROTOTYPE TO TEST THE AMS ALGORITHM 
*    --> available geometries: 'shell' (shell of interior radius a and exterior radius r)
*                              'jawbreaker' (alternation of shells that may be of two distinct materials)
*    --> available reactions: isotropic elastic scattering, absorption
*    --> isotropic point source of monokinetic particles
*       
***************************************************************************/

#define _USE_MATH_DEFINES

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <math.h>
#include <algorithm>
#ifdef HAS_ROOT
#include <TMath.h>
#include <TFile.h>
#include <TTree.h>
#include <TRandom3.h>
#include <TF1.h>
#include <TGraph.h>
#endif
#include <time.h>
#include <string.h>
#include "pointvector.hh"
#include "particle.hh"
#include "geometry.hh"
#include "shell.hh"
#include "jawbreaker.hh"
#include "delta.hh"
#include "bypass.hh"

#include <random>


using namespace std;
Geometry* the_geometry;
Randomizer* the_rand = new Randomizer(2);
//Randomizer* the_rand = new Randomizer(clock());

int main(int argc, char *argv[])
{
// Default parameters of the simulation
    int interrupted=0;
    int k,i;
    double k_pct = 1.;
    int res_file=0;
    int header=0;
    double sigma_tot(1.);
    double sigma_tot2(2.);
    int S=1;                          // simulation number (to be fixed by argument in case of multiple simulations)
    mode ams_mode = position;
    int ams_brake=0;
    geom geometry = shell;

    double sigma_s(0.5), sigma_s2(0.5), r_min(10.), r_max(10.1);
    double front_inf(r_min),front_sup(r_max);
    int nb_batch(100), batch_size(1000);
    stringstream filename;
    int ams_flag = 0;
#ifdef HAS_ROOT
    int root_flag=1;
#else
    int root_flag=0;
#endif
    if(argc==1) {
        cout<<" -------------  "<<endl;
        cout<<"   Minimal syntax: \"./menhir AMS\" or \"./menhir Analog\" <geometry>"<<endl;
        cout<<" -------------  "<<endl;
        cout<<"   Full syntax for AMS:"<<endl;
        cout<<"       ./menhir AMS <geometry> <param1> <param2> <...> <nb_batch> <batch_size> [k%] [ams_mode] [filename]"<<endl;
        cout<<"   Full syntax for analog:"<<endl;
        cout<<"       ./menhir Analog <geometry> <param1> <param2> <...> <nb_batch> <batch_size> [filename]"<<endl;
        cout<<" -------------  "<<endl;
        return 1;
    }
    if(strcmp(argv[1],"AMS")==0)
        ams_flag = 1;
    else if(strcmp(argv[1],"Analog")!=0 && strcmp(argv[1],"Theory")!=0){
        cout<<"First argument should be 'AMS' or 'Analog'. RTFM."<<endl;
        return 1;
    }
    if(argc>2) {
        if(strcmp(argv[2],"shell")==0||strcmp(argv[2],"Shell")==0||strcmp(argv[2],"SHELL")==0)
            geometry = shell;
        else if(strcmp(argv[2],"jawbreaker")==0||strcmp(argv[2],"Jawbreaker")==0||strcmp(argv[2],"JAWBREAKER")==0)
            geometry = jawbreaker;
        else if(strncmp(argv[2],"delta",5)==0||strncmp(argv[2],"Delta",5)==0||strncmp(argv[2],"DELTA",5)==0)
            geometry = delta;
        else if(strncmp(argv[2],"bypass",5)==0||strncmp(argv[2],"Bypass",5)==0||strncmp(argv[2],"BYPASS",5)==0)
            geometry = bypass;
        else {
            cout<<"Unknown geometry! (Please use SHELL, JAWBREAKER, BYPASS or DELTA"<<endl;
            return 1;
        }
    }
    if(geometry==shell) {
        if(argc>8) {
            sigma_tot  = atof(argv[3]);
            sigma_s    = atof(argv[4]);
            r_min      = atof(argv[5]);
            r_max      = atof(argv[6]);
            nb_batch   = atoi(argv[7]);
            batch_size = atoi(argv[8]);
            if(ams_flag && argc>9) {
                k_pct = atof(argv[9]);
            }
            if(ams_flag && argc>10) { 
                std::string reading(argv[10]);
                if(reading.substr(reading.find("-")+1)=="brake")
                    ams_brake = 100;
                if (reading.compare(0,5,"exact",0,5)==0)
                    ams_mode = exact;
                else if (reading.compare(0,3,"position",0,3)==0)
                    ams_mode = toposition;
                else if (reading.compare(0,3,"direction",0,3)==0)
                    ams_mode = direction;
                else if (reading.compare(0,4,"xabs",0,4)==0)
                    ams_mode = x;
                else if (reading.compare(0,1,"x",0,1)==0)
                    ams_mode = xabs;
                else {
                    cout<<"You entered \""<<argv[10]<<"\"."<<endl;
                    cout<<"AMS modes available: exact, direction, position, x."<<endl;
                    return 1;
                }
            }
            if(argc>2*ams_flag+9) {
                filename<<argv[2*ams_flag+9];
            }
        }
        else if(ams_flag){
            cout<<"Expected at least 6 arguments (<geometry> <sigma_tot> <sigma_s> <inner radius> <outer radius> <nb_batch> <batch_size> [k(%)] [ams_mode] [filename])"<<endl;
            return 1;
        }
        else {
            cout<<"Expected at least 6 arguments (<geometry> <sigma_tot> <sigma_s> <inner radius> <outer radius> <nb_batch> <batch_size> [filename])"<<endl;
            return 1;
        }
    }
    else {
        if(argc>10) {
            sigma_tot  = atof(argv[3]);
            sigma_s    = atof(argv[4]);
            sigma_tot2 = atof(argv[5]);
            sigma_s2   = atof(argv[6]);
            r_min      = atof(argv[7]);
            r_max      = atof(argv[8]);
            nb_batch   = atoi(argv[9]);
            batch_size = atoi(argv[10]);
            if(ams_flag && argc>11)
                k_pct = atof(argv[11]);
            if(ams_flag && argc>12) { 
                if (strncmp(argv[12],"exact",5)==0)
                    ams_mode = exact;
                else if (strncmp(argv[12],"position",3)==0)
                    ams_mode = position;
                else if (strncmp(argv[12],"toposition",5)==0 && geometry==bypass)
                    ams_mode = toposition;
                else if (strncmp(argv[12],"direction",3)==0)
                    ams_mode = direction;
                else if (strncmp(argv[12],"x",3)==0)
                    ams_mode = x;
                else if (strncmp(argv[12],"xabs",3)==0)
                    ams_mode = xabs;
                else {
                    cout<<"You entered \""<<argv[12]<<"\"."<<endl;
                    cout<<"AMS modes available: exact, direction, position."<<endl;
                    return 1;
                }
            }
            if(argc>2*ams_flag+11) {
                filename<<argv[2*ams_flag+9];
            }
        }
        else if(ams_flag){
            cout<<"Expected at least 6 arguments (<geometry> <sigma_tot_1> <sigma_s_1> <sigma_tot_2> <sigma_s_2> <inner radius> <outer radius> <nb_batch> <batch_size> [k] [ams_mode] [filename])"<<endl;
            return 1;
        }
        else {
            cout<<"Expected at least 6 arguments (<geometry> <sigma_tot_1> <sigma_s_1> <sigma_tot_2> <sigma_s_2> <inner radius> <outer radius> <nb_batch> <batch_size> [filename])"<<endl;
            return 1;
        }
    }

    // Setting program parameters from input
    k = int(floor((k_pct*double(batch_size)/100.)));
    if(k<1) k++;
    if(!ams_brake) ams_brake=k;
    S = nb_batch; 

    int current_iteration=0;
    double ams_level=0.;              
    int ams_iterations=0,simu_nb=1;
    double p=1;
    double track_AMS,coll_AMS;
    
    double t_batch,mean_track(0.),sigma_track(0.), mean_p(0.), t_simu(0.);

    std::ostream *output = &cout;
    if(filename.str().size()!=0) {
      res_file=1;
      // If filename is not *.root, replace standard output by fstream
      // and disable root output.
      if(filename.str().substr(filename.str().find_last_of(".") + 1) != "root") {
          output = new ofstream(filename.str().c_str());
          output->setf(ios::scientific, ios_base::floatfield);
          root_flag = 0;
      }
    }
    else
        filename << "./last_menhir_run.root";

#ifdef HAS_ROOT
    // ROOT Trees
    char root_file[256];
    TFile *f;
    TTree *EstimatorTree;
    if(root_flag) {
        sprintf(root_file, "%s",filename.str().c_str());
        f = new TFile(root_file,"RECREATE");
        EstimatorTree = new TTree("MENHIR_Output", "MENHIR output");
        EstimatorTree->Branch("batch_size",&batch_size,"batch_size/I");
        EstimatorTree->Branch("k",&k,"k/I");
        EstimatorTree->Branch("sigma_tot",&sigma_tot,"sigma_tot/D");
        EstimatorTree->Branch("sigma_s",&sigma_s,"sigma_s/D");
        EstimatorTree->Branch("sigma_tot2",&sigma_tot2,"sigma_tot2/D");
        EstimatorTree->Branch("sigma_s2",&sigma_s2,"sigma_s2/D");
        EstimatorTree->Branch("r_min",&front_inf,"r_min/D");
        EstimatorTree->Branch("r_max",&front_sup,"r_max/D");
        EstimatorTree->Branch("batch_num",&simu_nb,"batch_num/I");
        EstimatorTree->Branch("ams_iterations",&ams_iterations,"ams_iterations/I");
        EstimatorTree->Branch("score_p",&p,"score_p/D");
        EstimatorTree->Branch("score_track",&track_AMS,"score_track/D");
        EstimatorTree->Branch("score_coll",&coll_AMS,"score_coll/D");
        EstimatorTree->Branch("t_batch",&t_batch,"t_batch/D");
        EstimatorTree->Branch("mean_track",&mean_track,"mean_track/D");
        EstimatorTree->Branch("sigma_track",&sigma_track,"sigma_track/D");
    }
#endif


// Getting theoretical value
 stringstream command;
 stringstream theoryfile;
 clock_t t = clock();
 float theory=0.;
 if (sigma_tot==1.) {
     theoryfile<<".theory."<<t<<".tmp";
     command<<"python ../theory/theory.py "<<sigma_s/sigma_tot<<" "<<r_min<<" "<<r_max<<" > .theory."<<t<<".tmp";
     system(command.str().c_str());
     FILE *theory_file = fopen(theoryfile.str().c_str(),"r");
     fscanf(theory_file,"%f",&theory);
     fclose(theory_file);
     command.str("");
     command<<"rm .theory."<<t<<".tmp";
     system(command.str().c_str());
 }


// Declaration of the vectors holding the scores
    vector<double> mean;
    vector<double> sum2;
    vector<double> sigma;

    int edit = int(S/100.);  
    if(edit<1)
        edit = 1;
    // LOOP ON BATCHES
    for(simu_nb=1;simu_nb<=S;simu_nb++) {
        // Preparing time calculation
            t = clock();
        // Declaration of the geometry (resets scores)
        if(geometry==shell) {
            the_geometry = new Shell(sigma_tot, sigma_s, r_min, r_max, ams_mode); 
            if(mean.size()==0) {
                mean.push_back(0.);
                sum2.push_back(0.);
                sigma.push_back(0.);
            }
        }
        else if(geometry==jawbreaker) {
            the_geometry = new Jawbreaker(sigma_tot, sigma_s, sigma_tot2, sigma_s2, r_min, r_max, ams_mode); 
            if(mean.size()==0) {
                for(i=0;i<the_geometry->get_nb_volumes();i++) {
                    mean.push_back(0.);
                    sum2.push_back(0.);
                    sigma.push_back(0.);
                }
            }
        }
        else if(geometry==delta) {
            the_geometry = new Delta(sigma_tot, sigma_s, sigma_tot2, sigma_s2, r_min, r_max, ams_mode); 
            if(mean.size()==0) {
                for(i=0;i<the_geometry->get_nb_volumes();i++) {
                    mean.push_back(0.);
                    sum2.push_back(0.);
                    sigma.push_back(0.);
                }
            }
        }

        // Initialization of the particles
        int counter(0);
        vector<Particle> part;
        vector<Particle> dead;
        vector<double> importance(batch_size,0.);
        vector<double> sorted(batch_size,0.);
        vector<int> rejected;
        vector<int> survivors;   

        if(!header && ams_flag) {
            header = 1;
            if(!res_file) {
                (*output)<<"****************************************************"<<endl;
                (*output)<<"#     /\\      "<<endl;  
                (*output)<<"#    /  \\                   MENHIR v2.0"<<endl;
                (*output)<<"#   /    \\    "<<endl;
                (*output)<<"#  (      )    -------------------------------------"<<endl;
                (*output)<<"#  |      |                AMS simulation"<<endl; 
                (*output)<<"#  (      )    -------------------------------------"<<endl;
                (*output)<<"#   \\____/"<<flush;  
            }
            else if (!root_flag) {
                (*output)<<"# ";  
                for(i=0;i<argc;i++) (*output)<<" "<<argv[i];
                (*output)<<"\n#         flux     sigma(\%)";
            }
        }
        else if (!header) {
            header = 1;
            if(!res_file) {
                (*output)<<"****************************************************"<<endl;
                (*output)<<"#     /\\      "<<endl;  
                (*output)<<"#    /  \\                   MENHIR v2.0"<<endl;
                (*output)<<"#   /    \\    "<<endl;
                (*output)<<"#  (      )    -------------------------------------"<<endl;
                (*output)<<"#  |      |                Analog simulation"<<endl; 
                (*output)<<"#  (      )    -------------------------------------"<<endl;
                (*output)<<"#   \\____/"<<flush;  
            }
            else if (!root_flag) {
                (*output)<<"# ";  
                for(i=0;i<argc;i++) (*output)<<" "<<argv[i];
                (*output)<<"\n#         flux     sigma(\%)";
            }
        }
        if((simu_nb==2 || (simu_nb-1)%edit==0)&&simu_nb>1) {
            if(!res_file) {
                (*output)<<"\r                                                                                          "<<flush;
                (*output)<<"\r#   \\____/ "<<" Batch "<<setprecision(0)<<setw(5)<<left<<simu_nb-1
                                      <<"  score = "<<setprecision(4) <<scientific<< mean_track
                                      <<" (+/- "<<setprecision(6)<<fixed<<sigma_track/mean_track/sqrt(simu_nb)*100.<<"%) "<<flush;
            }
            else if (!root_flag) {
                (*output)<<"\n"<<setprecision(0)<<setw(5)<<left<<simu_nb-1<<"  "<<setprecision(4) <<scientific<< mean_track <<" "<<setprecision(6)<<fixed<<sigma_track/mean_track*100.;
            }
        }

        //  AMS ALGORITHM
        p = 1;
        current_iteration = 0;
        k = int(floor((k_pct*double(batch_size)/100.)));
        for(i=0;i<batch_size;i++) {
            part.push_back(Particle());
            counter++;
            while(part.at(i).is_alive())
                part.at(i).fly();
            importance.at(i)=part.at(i).get_importance();
        }
        nth_element(sorted.begin(),sorted.begin()+k-1,sorted.end());
        ams_level=sorted.at(k-1);
        current_iteration++;

        if (ams_flag && ams_level<DBL_MAX) {
            // Iterations j=1 to j=N
            while(ams_level<DBL_MAX) {
                rejected.clear();
                survivors.clear();
                for(i=0;i<batch_size;i++) {
                    if(importance.at(i)<=ams_level)
                        rejected.push_back(i);
                    else
                        survivors.push_back(i);
                }
                if(survivors.size()==0) {
                    interrupted += 1;
                    break;
                }
                unsigned int l;
                for(l=0;l<rejected.size();l++) {
                    i = rejected.at(l);
                    // a particle is randomly selected among the ones that are kept
                    unsigned int copied;
                    //copied = (int)floor(the_rand->uniform(0.,1.)*(double)survivors.size());
                    copied = the_rand->uniform(0.,survivors.size()-1.);
                    if(copied==survivors.size())
                        copied--;
                    switch(ams_mode) {
                      case direction:
                      case x:
                      case xabs:
                      case position:
                        part.at(i).copy_nxtcoll(part.at(survivors.at(copied)),ams_level);
                        break;
                      case exact:
                        part.at(i).copy_threshold(part.at(survivors.at(copied)),ams_level);
                        break;
                    }
                    while(part.at(i).is_alive()) {
                        part.at(i).fly();
                    }
                    importance.at(i)=part.at(i).get_importance();
                    if(importance.at(i)==DBL_MAX and k!=ams_brake) {
                        k = ams_brake;
                    }
                }
                p*=(1.-rejected.size()/double(batch_size));
                sorted.assign(importance.begin(),importance.end());
                nth_element(sorted.begin(),sorted.begin()+k-1,sorted.end());
                ams_level=sorted.at(k-1);
                current_iteration++;
            }
        }
        // COMPUTATION OF THE ESTIMATORS
        ams_iterations = current_iteration-1;
        mean_p+=p;

        //  computation of calculation duration
        t = clock() - t;
        t_batch = (double)t/CLOCKS_PER_SEC;
        t_simu+=t_batch;
        double prev_score;  
        for(i=0;i<mean.size();i++) {
            track_AMS = p*the_geometry->get_track(i)/double(batch_size);
            coll_AMS = p*the_geometry->get_coll(i)/double(batch_size);
            prev_score = mean.at(i);
            mean.at(i) = (prev_score*((double)simu_nb-1.)+track_AMS)/(double)simu_nb;
            sum2.at(i) += track_AMS*track_AMS;
            sigma.at(i) = sqrt(sum2.at(i)/(double)simu_nb-mean.at(i)*mean.at(i));
            mean_track = mean.at(i);
            sigma_track = sigma.at(i);
#ifdef HAS_ROOT
            if(root_flag) {
                front_inf = the_geometry->get_frontier_inf(i);
                front_sup = the_geometry->get_frontier_sup(i);
                EstimatorTree->Fill();
            }
#endif
        }
        delete the_geometry;
    } //END OF LOOP ON SIMULATION NB
    simu_nb--;
    mean_p /= (double)simu_nb;

    if(!res_file) {
        (*output)<<"\r                                                                                          "<<flush<<"\r#   \\____/     ";
        (*output)<<"score = "<< setprecision(4) <<scientific<< mean_track <<fixed<<" std.dev. = "<<sigma_track/sqrt(simu_nb-1)/mean_track*100.<<"%"<<endl;
        if(theory>0.)
            (*output)<<"#              theory: "<<setprecision(4) <<scientific<<theory
            <<fixed<<" deviation: "<<abs((theory-mean_track)/sigma_track*sqrt(simu_nb-1))<<" std.dev."<<endl;
        else
            (*output)<<"#              theory: not available (sigtot != 1)"<<endl;
        (*output)<<"****************************************************"<<endl;
        if(interrupted)
            (*output)<<"time: "<<t_simu<<" seconds. "<<interrupted<<" extinction(s). "<<endl<<endl;
        else
            (*output)<<"time: "<<t_simu<<" seconds. "<<endl;
        (*output)<<"fom:  "<<(simu_nb-1)/t_simu/sigma_track/sigma_track/1000000.<<" M"<<endl;
    }
    else if (!root_flag) {
        if(theory>0.)
            (*output)<<"\n# theory: "<<setprecision(4) <<scientific<<theory
            <<fixed<<" deviation: "<<abs((theory-mean_track)/sigma_track*sqrt(simu_nb-1))<<" std.dev."<<endl;
        else
            (*output)<<"#              theory: not available (sigtot != 1)"<<endl;
        if(interrupted)
            (*output)<<"# time: "<<t_simu<<" seconds. "<<endl;
        else
            (*output)<<"# time: "<<t_simu<<" seconds. "<<interrupted<<" extinctions(s)."<<endl;
    }
#ifdef HAS_ROOT
    if(root_flag) {
        EstimatorTree->Write();
        f->Close();
    }
#endif
    return 0;
}

