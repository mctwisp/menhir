#ifndef DEF_BYP
#define DEF_BYP
#include "geometry.hh"
#include "particle.hh"
#include <math.h>
#include <stdlib.h>
/*
 *
 * The 'bypass' problem consists in an extruded box filled with a material of cross section sigma_1,
 * with leakage boundary conditions. The box is 10cmx10cm. An isotropic particle source is placed at
 * one corner of the box and the flux is detected inside an infinite cylinder of 1cm in diameter placed
 * at the opposite corner. Between the source and the detector is placed an absorbent block of cross
 * section sigma_2.
 * y
 * |  _ _ _ _ _ _ _ _ _ _ _ _
 *   |                    _  |
 *   | 2   _____3_______ (8) |
 *   |    |             |    |
 *   |    |             |    |
 *   |  1 |     7       |    |
 *   |    |             | 6  |
 *   |    |             |    |
 *   |    |_____________|    |
 *   | 0.         4       5  |
 *   |_ _ _ _ _ _ _ _ _ _ _ _|
 * -10   -6      0      6    10  
 *                              -x
 */
class Bypass: public Geometry
{
    public:
        void   transport(Particle& part);
        double get_distance_to_bore(const Particle& part) const;
        double get_distance_to_target(const Particle& part) const;
        double get_distance_to_frontier(const Particle& part) const;
        double get_max_flight(Particle& part) const;
        void   do_collision(Particle& part);
        double get_importance(const Particle& part) const;
        double get_track(int ivol) const;
        double get_coll(int ivol) const;
        void   update_scores(const Particle& part);

        double sigma_tot_he;
        double sigma_abs_he;
        double sigma_tot_b;
        double sigma_abs_b;
        double front_pos;
        double target_max;
        double target_min;
        double target_size;
        Point target_point;
        double bore_pos;
        int bore_vol;
        std::vector<double> frontiers;
        std::vector<double> score_track;
        std::vector<double> score_coll;
        Bypass(double tot1, double scatt1, double tot2, double scatt2, double mini, double maxi, mode m);
};



#endif
