#ifndef DEF_POINTVECTOR
#define DEF_POINTVECTOR

#define _USE_MATH_DEFINES
#include <float.h>
#include <math.h>
#include <vector>
#include <stdlib.h>
#ifdef HAS_ROOT
#include <TRandom3.h>
#else
#include <random>
#endif

class Geometry;
extern Geometry* the_geometry;

class Randomizer
{
    private:
        int seed;
#ifdef HAS_ROOT
        TRandom3* root_rand;
#else
        std::mt19937* c11_rand;
#endif

    public:
        Randomizer(int);
        double uniform(double, double);    
        int uniform(int, int);    
};
extern Randomizer* the_rand;

class Vector
{
    public:
        double x;
        double y;
        double z;
        double get_norm() const;
        Vector scale(double a);
        void normalize();
        void add(Vector other);
        double dot(Vector const& other) const;
        Vector cross(Vector other);
        Vector orthogonal() const;
        Vector operator=(Vector const& b);
        Vector();
        Vector(double x_i, double y_i, double z_i);
        Vector(Vector const& other);
};

Vector sample_direction();
Vector sample_2d_direction();
Vector operator+(Vector const& a, Vector const& b);
Vector operator-(Vector const& a, Vector const& b);
Vector operator*(double const& a, Vector const& b);
bool operator==(Vector const& a, Vector const& b);

class Point
{
    public:
        double x;
        double y;
        double z;

        void move(Vector Dir, double Dist);
        double get_r() const;
        double get_r2() const;
        Point operator=(Point const& b);
        Point();
        Point(double x_i, double y_i, double z_i);
        Point(Point const& other);
};

Point get_point(Point const& P1,Point const& P2,double const& Q);
double get_dist(Point const& P1,Point const& P2);
bool operator==(Point const& a, Point const& b);

#endif

