#ifndef DEF_DELTA
#define DEF_DELTA
#include "geometry.hh"
#include "particle.hh"
#include <math.h>
#include <stdlib.h>

class Delta: public Geometry
{
    public:
        void   transport(Particle& part);
        void   do_collision(Particle& part);
        double get_importance(const Particle& part) const;
        double get_track(int ivol) const;
        double get_coll(int ivol) const;
        int    get_nb_volumes() const;
        double get_frontier_inf(int ivol) const;
        double get_frontier_sup(int ivol) const;
        void   update_scores(const Particle& part);

        double sigma_tot_odd;
        double sigma_abs_odd;
        double sigma_tot_even;
        double sigma_abs_even;
        double sigma_maj;
        double layer_width;
        std::vector<double> frontiers;
        std::vector<double> score_track;
        std::vector<double> score_coll;
        Delta(double tot1, double scatt1, double tot2, double scatt2, double mini, double maxi, mode m);
};



#endif
