#include "particle.hh"
#include "pointvector.hh"
#include "geometry.hh"
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>


using namespace std;
ParticleState::ParticleState()
{
    position = Point(0.,0.,0.);
    direction= Vector(0.,0.,0.);
    energy = -1;
    weight = 0.;
    importance = -DBL_MAX;
    volume = -1;
    alive = false;
}
ParticleState::ParticleState(const Point& pos, const Vector& dir, const double& E, const double& w, const double& I, const int& vol, const bool& life):
position(pos), direction(dir), energy (E), weight(w), importance(I), volume(vol), alive(life)
{
}
ParticleState ParticleState::operator=(const ParticleState& b)
{
    position = b.position;
    direction = b.direction;
    energy = b.energy;
    weight = b.weight;
    importance = b.importance;
    volume = b.volume;
    alive = b.alive;
    return *this;
}

const Point& Particle::get_position() const
{
    return position;
}
const Vector& Particle::get_direction() const
{
    return direction;
}
double Particle::get_energy() const
{
    return energy;
}
double Particle::get_weight() const
{
    return weight;
}
double Particle::get_importance() const
{
    return importance;
}
bool Particle::is_alive() const
{
    return alive;
}

// This method performs transport+collision+change of importance
void Particle::fly()
{    
    double point_importance;
    the_geometry->transport(*this);
    if(secondary)
        return;
    point_importance = the_geometry->get_importance(*this); 
    if (point_importance>importance) {
        importance = point_importance;
        history.push_back(ParticleState(position,direction,energy,weight,importance,volume,alive));
    }
    else if(the_geometry->ams_mode==exact)
        history.push_back(ParticleState(position,direction,energy,weight,importance,volume,alive));
}

//  Exact duplication method
void Particle::copy_threshold(const Particle& model, const double& threshold)
{
    unsigned int it;
    for(it=1;it<model.history.size();it++) {
        if(model.history.at(it).importance>=threshold) {
            reset_history();
            position = get_point(model.history.at(it-1).position,model.history.at(it).position,sqrt(threshold));
            direction = model.history.at(it-1).direction;
            energy = model.history.at(it-1).energy;
            weight = model.history.at(it-1).weight;
            importance = the_geometry->get_importance(*this); 
            volume = model.history.at(it-1).volume;
            alive = true;
            history.push_back(ParticleState(position,direction,energy,weight,importance,volume,alive));
            break;
        }
    }
}

//  Next coll duplication method
void Particle::copy_nxtcoll(const Particle& model, const double& threshold)
{
    unsigned int it;
    for(it=0;it<model.history.size();it++) {
        if(model.history.at(it).importance>threshold) {
             reset_history();
             position = model.history.at(it).position;
             direction = model.history.at(it).direction;
             energy = model.history.at(it).energy;
             weight = model.history.at(it).weight;
             importance = model.history.at(it).importance;
             volume = model.history.at(it).volume;
             alive = model.history.at(it).alive;
             history.push_back(ParticleState(position,direction,energy,weight,importance,volume,alive));
             break;
        }
    }
    //if(importance==DBL_MAX)
    //    the_geometry->update_scores(*this);
}

void Particle::reset_history()
{
    importance = -DBL_MAX;
    history.clear();
}
Particle::Particle(const Particle& part, int split): weight(part.weight), importance(-DBL_MAX), alive(true)
{
    position = part.position;
    energy = part.energy;
    if(energy>0.25)
        direction = part.direction.orthogonal();
    else
        direction = part.direction;
    volume = part.volume;
    secondary = true;
}
Particle::Particle(): weight(1.), importance(-DBL_MAX), alive(true)
{
    position = the_geometry->get_source_point();
    direction = the_geometry->get_source_dir();
    energy = the_geometry->get_source_energy();
    volume = the_geometry->get_source_vol();
    secondary = false;
    history.push_back(ParticleState(position,direction,energy,weight,importance,volume,alive));
}
