#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

Randomizer::Randomizer(int s=0)
{
    seed=s;
#ifdef HAS_ROOT
    root_rand = new TRandom3(seed);
#else
    c11_rand = new mt19937(seed);
#endif
}

double Randomizer::uniform(double a, double b)
{
    double result;
#ifdef HAS_ROOT
    result = root_rand->Uniform(a,b);
#else
    uniform_real_distribution<double> real_dist(a, b);
    result = real_dist(*c11_rand);
#endif
    return result;
}

int Randomizer::uniform(int a, int b)
{
    double result;
#ifdef HAS_ROOT
    result = a+root_rand->Integer(b+1-a);
#else
    uniform_int_distribution<int> int_dist(a, b);
    result = int_dist(*c11_rand);
#endif
    return result;
}



Point::Point():x(0.), y(0.), z(0.)
{}
Point::Point(double x_i, double y_i, double z_i):x(x_i), y(y_i),z(z_i)
{}
Point::Point(Point const& other):x(other.x), y(other.y), z(other.z)
{}

void Point::move(Vector dir, double dist)
{
    x+=dist*dir.x;
    y+=dist*dir.y;
    z+=dist*dir.z;
}
double Point::get_r() const
{
    return sqrt(x*x+y*y+z*z);
}
double Point::get_r2() const
{
    return x*x+y*y+z*z;
}
Point Point::operator=(Point const& b)
{
    x = b.x;
    y = b.y;
    z = b.z;
    return *this;
}
bool operator==(Point const& a, Point const& b)
{
    bool output = false;
    if(a.x==b.x && a.y==b.y && a.z==b.z)
        output = true;
    return output;    
}

Vector::Vector():x(0), y(0), z(0)
{}
Vector::Vector(double x_i, double y_i, double z_i): x(x_i), y(y_i),z(z_i)
{}
Vector::Vector(Vector const& other): x(other.x), y(other.y), z(other.z)
{}

double Vector::get_norm() const
{
    return sqrt(x*x+y*y+z*z);
}
Vector Vector::scale(double a)
{
    Vector out;
    out.x = x*a;
    out.y = y*a;
    out.z = z*a;
    return out;
}
void Vector::normalize()
{
    double norm = this->get_norm();
    x*=1./norm;
    y*=1./norm;
    z*=1./norm;
}
void Vector::add(Vector other)
{
    x+=other.x;
    y+=other.y;
    z+=other.z;
}
double Vector::dot(Vector const& other) const
{
    return x*other.x+y*other.y+z*other.z;
}
Vector Vector::cross(Vector other)
{
    return Vector(y*other.z-other.y*z,z*other.x-other.z*x,x*other.y-other.x*y);
}
Vector Vector::orthogonal() const
{
        double x_o(1),y_o(1),z_o(0);
        if(x!=0.) {
            x_o = 0.; 
            y_o = 1.;
        }
        else {
            y_o = 0.;
            x_o = 1.;
        }

        //if(x!=0) x_o=-(y+z)/x;
        //else if(y!=0) y_o=-(x+z)/y;
        //else if(z!=0) z_o=-(y+x)/z;
        return Vector(x_o,y_o,z_o);
}
Vector Vector::operator=(Vector const& b)
{
    x = b.x;
    y = b.y;
    z = b.z;
    return *this;
}

Vector operator+(Vector const& a, Vector const& b)
{
    Vector Output;
    Output.x = a.x + b.x;
    Output.y = a.y + b.y;
    Output.z = a.z + b.z;
    return Output;    
}
Vector operator-(Vector const& a, Vector const& b)
{
    Vector Output;
    Output.x = a.x - b.x;
    Output.y = a.y - b.y;
    Output.z = a.z - b.z;
    return Output;    
}

Vector operator*(double const& a, Vector const& b)
{
    Vector Output;
    Output.x = a*b.x;
    Output.y = a*b.y;
    Output.z = a*b.z;
    return Output;    
}
bool operator==(Vector const& a, Vector const& b)
{
    bool Output = false;
    if(a.x==b.x && a.y==b.y && a.z==b.z)
        Output = true;
    return Output;    
}

Vector sample_direction()
{
    Vector direction;
    double mu, theta, phi;

    mu = the_rand->uniform(-1.,1.);
    phi = the_rand->uniform(0.,2.*M_PI);
    theta = acos(mu);
    direction = Vector(sin(theta)*cos(phi), sin(theta)*sin(phi),mu);
    direction.normalize();
    return direction;
}

Vector sample_2d_direction()
{
    Vector direction;
    double theta;
    theta = the_rand->uniform(0.,2.*M_PI);
    direction = Vector(cos(theta), sin(theta), 0.);
    return direction;
}

Point get_point(Point const& P1,Point const& P2,double const& Q)
{
    double X = P2.x-P1.x;
    double Y = P2.y-P1.y;
    double Z = P2.z-P1.z;
    double a = pow(X,2)+pow(Y,2)+pow(Z,2);
    double b = 2.*(X*P1.x+Y*P1.y+Z*P1.z);
    double c = pow(P1.x,2) + pow(P1.y,2) + pow(P1.z,2) - pow(Q,2);
    
    double delta = pow(b,2)-4.*a*c;
    double t = (-b-sqrt(delta))/(2.*a);
    if(t<0 || t>1)
        t = (-b+sqrt(delta))/(2.*a);
    double nX=P1.x + X*t;
    double nY=P1.y + Y*t;
    double nZ=P1.z + Z*t;
    return Point(nX,nY,nZ);
}

double get_dist(Point const& P1,Point const& P2)
{
    double X = P2.x-P1.x;
    double Y = P2.y-P1.y;
    double Z = P2.z-P1.z;
    return X*X+Y*Y+Z*Z;
}
