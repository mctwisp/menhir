#include "delta.hh"
#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

void Delta::transport(Particle& part)
{ 
    double ksi, dist;

    ksi = the_rand->uniform(0.,1.);
    dist = -log(1.-ksi)/sigma_maj;
    part.position.move(part.direction,dist);
    part.volume = int(part.position.get_r()/layer_width);
    this->do_collision(part);
}

void Delta::do_collision(Particle& part)
{
    double ksi = the_rand->uniform(0.,1.);
    double sigma_tot(sigma_tot_odd),sigma_abs(sigma_abs_odd);
    if(part.volume%2==0) {
        sigma_abs = sigma_abs_even;
        sigma_tot = sigma_tot_even;
    }

    // delta tracking rejection
    if(ksi>sigma_tot/sigma_maj)
        return;

    // collision accepted
    if(part.volume>-1 && part.volume<=target_vol)
        score_coll.at(part.volume)+=1./sigma_tot;
    ksi = the_rand->uniform(0.,1.);
    if(ksi<sigma_abs/sigma_tot) {
        part.alive = false;
        part.direction = Vector(0.,0.,0.);
    }
    else
        part.direction = sample_direction();
}
double Delta::get_importance(const Particle& part) const
{
    double importance;
    if(part.volume==target_vol)
        importance = DBL_MAX;
    else if(part.alive==false && this->ams_mode!=exact)
        importance = -DBL_MAX;
    else if(this->ams_mode==direction) {
        double xx = part.position.x+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.x;
        double yy = part.position.y+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.y;
        double zz = part.position.z+1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2))*part.direction.z;
        importance = sqrt(xx*xx+yy*yy+zz*zz);
    }
    else if(this->ams_mode==x) 
        importance = part.position.x;
    else if(this->ams_mode==xabs)
        importance = abs(part.position.x);
    else
        importance = part.position.get_r();
    return importance;
}
double Delta::get_track(int ivol) const
{
    return score_coll.at(ivol);
}
double Delta::get_coll(int ivol) const
{
    return score_coll.at(ivol);
}
int Delta::get_nb_volumes() const
{
    return target_vol+1;
}
double Delta::get_frontier_inf(int ivol) const
{
    return frontiers.at(ivol);
}
double Delta::get_frontier_sup(int ivol) const
{
    return frontiers.at(ivol+1);
}
void Delta::update_scores(const Particle& part)
{
    if(part.volume==target_vol)
        score_coll.at(target_vol)+=1./(sigma_tot_odd*(part.volume%2)+sigma_tot_even*(1-part.volume%2));
}

Delta::Delta(double tot1, double scatt1, double tot2, double scatt2, double mini, double maxi, mode m):
    sigma_tot_odd(tot1), sigma_abs_odd(tot1-scatt1), sigma_tot_even(tot2), sigma_abs_even(tot2-scatt2)
{
    type          = delta;
    ams_mode = m;
    sigma_maj     = max(sigma_tot_even,sigma_tot_odd);
    source_point  = Point(0,0,0);
    source_dir    = sample_direction();
    source_energy = 1.;
    source_vol    = -1;
    frontiers.clear();
    score_track.clear();
    score_coll.clear();
    layer_width = maxi-mini;
    int nb_layers = floor(mini/layer_width);
    double x = mini-(double) nb_layers*layer_width;
    if(x==0) source_vol = 0;
    for(int i=0;i<nb_layers+1;i++) {
        frontiers.push_back(x+i*layer_width);
        score_track.push_back(0.);
        score_coll.push_back(0.);
    }
    frontiers.push_back(maxi);
    score_track.push_back(0.);
    score_coll.push_back(0.);
    target_vol = nb_layers;
}
