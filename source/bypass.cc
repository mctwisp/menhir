#include "bypass.hh"
#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

void Bypass::transport(Particle& part)
{ 
    double sigma_tot(sigma_tot_he), ksi, dist, max_dist;

    ksi = the_rand->uniform(0.,1.);
    if(part.volume==bore_vol)
        sigma_tot = sigma_tot_b;

    dist = -log(1.-ksi)/sigma_tot;
    max_dist = get_max_flight(part);
    if(dist<max_dist) {
        part.position.move(part.direction,dist);
        if(part.volume==target_vol) {
            score_track[0]+=dist;
            score_coll[0]+=1./sigma_tot;
        }
        this->do_collision(part);
    }
    else if (part.next_volume==-1) {
        part.position.move(part.direction,max_dist);
        part.alive = false;
        part.direction = Vector(0.,0.,0.);
    }
    else {
        part.position.move(part.direction,max_dist);
        if(part.volume==target_vol)
            score_track[0]+=max_dist;
        part.volume = part.next_volume;
    }
}
double Bypass::get_distance_to_frontier(const Particle& part) const
{
    double dist;
    // Distance to right geom frontier (+x)
    dist = (front_pos-part.position.x)/part.direction.x;
    // Distance to left geom frontier (-x)
    dist = min(dist, (-front_pos-part.position.x)/part.direction.x);
    // Distance to top geom frontier (+y)
    dist = min(dist, (front_pos-part.position.y)/part.direction.y);
    // Distance to bottom geom frontier (-y)
    dist = min(dist, (-front_pos-part.position.y)/part.direction.y);
    return dist;
}
double Bypass::get_distance_to_bore(const Particle& part) const
{
    double dist=DBL_MAX;
    // Distance to right bore massive frontier (+x)
    dist = (bore_pos-part.position.x)/part.direction.x;
    if(abs(part.position.y+dist*part.direction.y)>bore_pos)
        dist=DBL_MAX;
    // Distance to left bore massive frontier (-x)
    dist = min(dist, (-bore_pos-part.position.x)/part.direction.x);
    if(abs(part.position.y+dist*part.direction.y)>bore_pos)
        dist=DBL_MAX;
    // Distance to top bore massive frontier (+y)
    dist = min(dist, (bore_pos-part.position.y)/part.direction.y);
    if(abs(part.position.x+dist*part.direction.x)>bore_pos)
        dist=DBL_MAX;
    // Distance to bottom bore massive frontier (-y)
    dist = min(dist, (-bore_pos-part.position.y)/part.direction.y);
    if(abs(part.position.x+dist*part.direction.x)>bore_pos)
        dist=DBL_MAX;
    return dist;
}
double Bypass::get_distance_to_target(const Particle& part) const
{
    double dist=DBL_MAX, proj;
    // Distance to right target frontier (x+1)
    dist = (target_max-part.position.x)/part.direction.x;
    proj = part.position.y+dist*part.direction.y;
    if(proj>target_max || proj<target_min)
        dist=DBL_MAX;
    // Distance to left target frontier (x-1)
    dist = min(dist, (target_min-part.position.x)/part.direction.x);
    proj = part.position.y+dist*part.direction.y;
    if(proj>target_max || proj<target_min)
        dist=DBL_MAX;
    // Distance to top target frontier (y+1)
    dist = min(dist, (target_max-part.position.y)/part.direction.y);
    proj = part.position.x+dist*part.direction.x;
    if(proj>target_max || proj<target_min)
        dist=DBL_MAX;
    // Distance to bottom target frontier (y-1)
    dist = min(dist, (target_min-part.position.y)/part.direction.y);
    proj = part.position.x+dist*part.direction.x;
    if(proj>target_max || proj<target_min)
        dist=DBL_MAX;
    return dist;
}
double Bypass::get_max_flight(Particle& part) const
{
    double d_frontier, d_bore, d_target, min_dist;
    if(part.volume==source_vol) {
        d_frontier = this->get_distance_to_frontier(part);
        d_bore = this->get_distance_to_bore(part);
        d_target = this->get_distance_to_target(part);
        if(d_bore<d_target) {
            if(d_bore<d_frontier) {
                min_dist = d_bore;
                part.next_volume = bore_vol;
            }
            else {
                min_dist = d_frontier;
                part.next_volume = -1;
            }
        }
        else {
            if(d_target<d_frontier) {
                min_dist = d_target;
                part.next_volume = target_vol;
            }
            else {
                min_dist = d_frontier;
                part.next_volume = -1;
            }
        }
    }
    else if(part.volume==bore_vol) {
        min_dist = this->get_distance_to_bore(part);
        part.next_volume = source_vol;
    }
    else if(part.volume==target_vol){
        min_dist = this->get_distance_to_target(part);
        part.next_volume = source_vol;
    }
    else if(part.volume==-1){
        cout<<"ERROR: Leaked particle believes it can fly!"<<endl;
    }
    else{
        cout<<"Particle without volume? (#"<<part.volume<<")"<<endl;
    }
    return min_dist;
}
void Bypass::do_collision(Particle& part)
{
    double ksi = the_rand->uniform(0.,1.);
    double sigma_tot(sigma_tot_he),sigma_abs(sigma_abs_he);
    if(part.volume==bore_vol) {
        sigma_abs = sigma_abs_b;
        sigma_tot = sigma_tot_b;
    }

    if(ksi<sigma_abs/sigma_tot) {
        part.alive = false;
        part.direction = Vector(0.,0.,0.);
    }
    else {
        part.direction = sample_2d_direction();
    }
}
double Bypass::get_importance(const Particle& part) const
{
    double importance;
    if(part.volume==target_vol)
        importance = DBL_MAX;
    else if(part.alive==false && this->ams_mode!=exact)
        importance = -DBL_MAX;
    else if(this->ams_mode==direction) {
        double sigma_tot(sigma_tot_he);
        if(part.volume==bore_vol) {
            sigma_tot = sigma_tot_b;
        }
        double xx = part.position.x+1./sigma_tot*part.direction.x;
        double yy = part.position.y+1./sigma_tot*part.direction.y;
        double zz = part.position.z+1./sigma_tot*part.direction.z;
        importance = sqrt(xx*xx+yy*yy+zz*zz);
    }
    else if(this->ams_mode==x) 
        importance = part.position.x;
    else if(this->ams_mode==xabs)
        importance = abs(part.position.x);
    else if(this->ams_mode==toposition)
        importance = get_dist(part.position, target_point);
    else
        importance = get_dist(part.position, source_point);
    return importance;
}
double Bypass::get_track(int ivol) const
{
    return score_track[0];
}
double Bypass::get_coll(int ivol) const
{
    return score_coll[0];
}
void Bypass::update_scores(const Particle& part)
{
}

Bypass::Bypass(double tot1, double scatt1, double tot2, double scatt2, double mini, double maxi, mode m):
    sigma_tot_he(tot1), sigma_abs_he(tot1-scatt1), sigma_tot_b(tot2), sigma_abs_b(tot2-scatt2)
{
    type          = bypass;
    ams_mode      = m;

    front_pos = 10;
    bore_pos = 6;
    double target_pos = 8;
    target_size = 2;
    target_min = target_pos-target_size/2.;
    target_max = target_pos+target_size/2.;
    target_point = Point(target_pos, target_pos, 0.);

    source_point = Point(-target_pos, -target_pos, 0.);
    source_dir    = sample_2d_direction();
    source_energy = 1.;
    source_vol    = 0;
    bore_vol      = 1;
    target_vol    = 2;
    score_track.clear();
    score_coll.clear();
    score_track.push_back(0.);
    score_coll.push_back(0.);
}
