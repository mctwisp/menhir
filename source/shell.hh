#ifndef DEF_SPHERE
#define DEF_SPHERE
#include "geometry.hh"
#include "particle.hh"
#include <math.h>
#include <stdlib.h>

class Shell: public Geometry
{
    public:
        void   transport(Particle& part);
        double get_distance_to_frontier(const Particle& part,double r_front) const;
        double get_max_flight(Particle& part) const;
        void   do_collision(Particle& part);
        double get_importance(const Particle& part) const;
        double get_track(int ivol) const;
        double get_coll(int ivol) const;
        void   update_scores(const Particle& part);
        double get_frontier_inf(int ivol) const;
        double get_frontier_sup(int ivol) const;

        double sigma_tot;
        double sigma_scatt;
        double sigma_abs;
        std::vector<double> frontiers;
        std::vector<double> score_track;
        std::vector<double> score_coll;
        Shell(double tot, double scatt, double mini, double maxi, mode m);
};



#endif
