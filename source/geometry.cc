#include "geometry.hh"
#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>
#include <csignal>

using namespace std;
Point   Geometry::get_source_point() const {return source_point;}
Vector  Geometry::get_source_dir() const {return source_dir;}
double  Geometry::get_source_energy() const {return source_energy;}
int     Geometry::get_source_vol() const {return source_vol;}
void    Geometry::transport(Particle& part)
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::transport()"<<endl;
    raise(SIGINT);
}
double  Geometry::get_max_flight(Particle& part) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_max_flight()"<<endl;
    raise(SIGINT);
    return(0.);
}
void    Geometry::do_collision(Particle& part)
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::do_collision()"<<endl;
    raise(SIGINT);
}
double  Geometry::get_importance(const Particle& part) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_importance()"<<endl;
    raise(SIGINT);
    return(0.);
}
double  Geometry::get_track(int ivol) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_track()"<<endl;
    raise(SIGINT);
    return(0.);
}
double  Geometry::get_coll(int ivol) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_coll()"<<endl;
    raise(SIGINT);
    return(0.);
}
int Geometry::get_nb_volumes() const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_nb_volumes()"<<endl;
    raise(SIGINT);
    return 0.;
}
double Geometry::get_frontier_inf(int ivol) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_frontier_inf()"<<endl;
    raise(SIGINT);
    return 0.;
}
double Geometry::get_frontier_sup(int ivol) const
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::get_frontier_sup()"<<endl;
    raise(SIGINT);
    return 0.;
}
void Geometry::update_scores(const Particle& part)
{
    cout<<"This function should not be called:"<<endl;
    cout<<"\tGeometry::update_scores()"<<endl;
    raise(SIGINT);
}
Geometry::~Geometry() {}
