#include "shell.hh"
#include "pointvector.hh"
#include<iostream>
#include <math.h>
#include <stdlib.h>

using namespace std;

void Shell::transport(Particle& part)
{ 
    double ksi, dist, max_dist;

    ksi = the_rand->uniform(0.,1.);
    dist = -log(1.-ksi)/sigma_tot;
    max_dist = get_max_flight(part);

    if(dist<max_dist) {
        part.position.move(part.direction,dist);
        if(part.volume==target_vol) {
            score_track[0]+=dist;
            score_coll[0]+=1./sigma_tot;
        }
        this->do_collision(part);
    }
    else {
        part.position.move(part.direction,max_dist);
        if(part.volume==target_vol)
            score_track[0]+=max_dist;
        part.volume = part.next_volume;
    }
}

double Shell::get_distance_to_frontier(const Particle& part, double r_front) const
{
    double r2, mu, delta, d1, d2;

    r2 = part.position.x*part.position.x+part.position.y*part.position.y+part.position.z*part.position.z; 
    mu = part.position.x*part.direction.x+part.position.y*part.direction.y+part.position.z*part.direction.z; 
    delta =  mu*mu - r2 + r_front*r_front;
    if (delta<0)
        return DBL_MAX;

    // distance calculation
    d1 = -mu-sqrt(delta);
    d2 = -mu+sqrt(delta);
    if(d1<=1.e-5)
        d1 = DBL_MAX;
    if(d2<=1.e-5)
        d2 = DBL_MAX;
    return min(d1,d2);
}

double Shell::get_max_flight(Particle& part) const
{
    double d1, d2, min_dist;
    switch(part.volume) {
      case 1:
          min_dist = this->get_distance_to_frontier(part,frontiers[0]);
          part.next_volume = 2;
          break;
      case 2:
          d1 = this->get_distance_to_frontier(part,frontiers[0]);
          d2 = this->get_distance_to_frontier(part,frontiers[1]);
          if(d1<d2) {
              min_dist = d1;
              part.next_volume = 1;
          }
          else {
              min_dist = d2;
              part.next_volume = 3;
          }
          break;
      case 3:
          min_dist = this->get_distance_to_frontier(part,frontiers[1]);
          part.next_volume = 2;
          break;
    }
    return min_dist;
}

void Shell::do_collision(Particle& part)
{
    double ksi = the_rand->uniform(0.,1.);
    if(ksi<sigma_abs/sigma_tot) {
        part.alive = false;
        part.direction = Vector(0.,0.,0.);
    }
    else {
        part.direction = sample_direction();
    }
}

double Shell::get_importance(const Particle& part) const
{
    double importance;
    if(part.volume==target_vol)
        importance = DBL_MAX;
    else if(part.alive==false && this->ams_mode!=exact)
        importance = -DBL_MAX;
    else if(this->ams_mode==direction) {
        double xx = part.position.x+1./sigma_tot*part.direction.x;
        double yy = part.position.y+1./sigma_tot*part.direction.y;
        double zz = part.position.z+1./sigma_tot*part.direction.z;
        importance = sqrt(xx*xx+yy*yy+zz*zz);
    }
    else if(this->ams_mode==x) 
        importance = part.position.x;
    else if(this->ams_mode==xabs)
        importance = abs(part.position.x);
    else {
        importance = part.position.get_r2();
    }
    return importance;
}

double Shell::get_track(int ivol) const
{
    return score_track[0];
}

double Shell::get_coll(int ivol) const
{
    return score_coll[0];
}

void Shell::update_scores(const Particle& part)
{
}

double Shell::get_frontier_inf(int ivol) const
{
    return frontiers[ivol];
}

double Shell::get_frontier_sup(int ivol) const
{
    return frontiers[ivol+1];
}

Shell::Shell(double tot, double scatt, double mini, double maxi, mode m): sigma_tot(tot), sigma_scatt(scatt), sigma_abs(tot-scatt)
{
    type          = shell;
    ams_mode      = m;
    source_point  = Point(0,0,0);
    source_dir    = sample_direction();
    source_energy = 1.;
    source_vol    = 1;
    frontiers.clear();
    frontiers.push_back(mini);
    frontiers.push_back(maxi);
    target_vol = 2;
    score_track.push_back(0.);
    score_coll.push_back(0.);
}
