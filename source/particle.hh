#ifndef DEF_PARTICLE
#define DEF_PARTICLE

#include "pointvector.hh"
//#include "geometry.hh"
#include <vector>
class ParticleState
{
    public:
        Point position;
        Vector direction;
        double energy;
        double weight;
        double importance;
        int volume;
        bool alive;
        ParticleState();
        ParticleState(const Point& pos, const Vector& dir, const double& E, const double& w, const double& I, const int& vol, const bool& life);
	ParticleState operator=(ParticleState const& b);
};

class Particle
{
    public: 
        const Point& get_position() const;
        const Vector& get_direction() const;
        double get_energy() const;
        double get_weight() const;
        double get_importance() const;
        bool is_alive() const;

        void fly();
        void copy_threshold(const Particle& model, const double& threshold);
        void copy_nxtcoll(const Particle& model, const double& threshold);
        void reset_history();
        Particle();
        Particle(const Particle& part, int split);

        Point position;
        Vector direction;
        double energy;
        double weight;
        double importance;
        int volume;
        int next_volume;
        bool alive;
        bool secondary;
        std::vector<ParticleState> history;
};


#endif
