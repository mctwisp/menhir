#ifndef DEF_MEDIUM
#define DEF_MEDIUM
#include "pointvector.hh"
#include "particle.hh"
#include <math.h>
#include <stdlib.h>

/// Enumeration for AMS mode
enum mode {
    exact     = 0,
    direction = 1,
    position  = 2,
    toposition= 3,
    x         = 4,
    xabs      = 5
};
enum geom {
    shell       = 0,
    jawbreaker  = 1,
    delta       = 2,
    bypass      = 3
};
enum score_name {
    flux_track        = 0,
    flux_coll         = 1
};
class Geometry
{
   protected:
        Point  source_point;
        Vector source_dir;
        double source_energy;
        int    source_vol;
   public:
        geom type;
        mode   ams_mode;
        int target_vol;
        virtual void       transport(Particle& part);
        virtual double     get_max_flight(Particle& part) const;
        virtual void       do_collision(Particle& part);
        virtual double     get_importance(const Particle& part) const;
        virtual double     get_track(int ivol=0) const;
        virtual double     get_coll(int ivol=0) const;
        virtual double     get_frontier_inf(int ivol) const;
        virtual double     get_frontier_sup(int ivol) const;
        virtual int        get_nb_volumes() const;
        virtual void       update_scores(const Particle& part);
        Point              get_source_point() const;
        Vector             get_source_dir() const;
        double             get_source_energy() const;
        int                get_source_vol() const;
        virtual            ~Geometry();
};

#endif
